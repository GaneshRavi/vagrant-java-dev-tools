wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/7u71-b14/jdk-7u71-linux-x64.tar.gz
tar -zxvf jdk-7u71-linux-x64.tar.gz
sudo mv -f jdk1.7.0_71/ /opt/
export JAVA_HOME=/opt/jdk1.7.0_71
ln -f -s $JAVA_HOME/bin/* /usr/bin/

sudo apt-get update

# Installing GIT and Maven
sudo apt-get -y install aptitude

sudo aptitude -y install git maven
